package com.adamkorzeniak.utils.exceptions.api.rest;

import com.adamkorzeniak.utils.exceptions.api.rest.model.DummyPerson;
import com.adamkorzeniak.utils.exceptions.model.exception.BusinessException;
import com.adamkorzeniak.utils.exceptions.test.utils.Messages;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.validation.Valid;

@EnableWebMvc
@EnableAutoConfiguration
@RestController
public class DummyTestController {

    @GetMapping("/test")
    public ResponseEntity<String> getEntity() {
        return ResponseEntity.ok().build();
    }

    @PostMapping("/test")
    public ResponseEntity<String> createEntity(@Valid DummyPerson dummyPerson) {
        return ResponseEntity.ok().build();
    }

    @GetMapping("/exception")
    public ResponseEntity<String> throwException() throws Throwable {
        throw new Throwable(Messages.UNEXPECTED_EXCEPTION_DUMMY_MESSAGE);
    }

    @GetMapping("/business-exception")
    public ResponseEntity<String> throwBusinessException() {
        throw new BusinessException(Messages.BUSINESS_EXCEPTION_MESSAGE) {
            @Override
            public String getCode() {
                return Messages.BUSINESS_EXCEPTION_CODE;
            }

            @Override
            public String getTitle() {
                return Messages.BUSINESS_EXCEPTION_TITLE;
            }
        };
    }
}
